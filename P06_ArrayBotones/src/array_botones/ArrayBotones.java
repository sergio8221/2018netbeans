/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package array_botones;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author Sergio Gutiérrez
 */
public class ArrayBotones extends javax.swing.JFrame {

    /**
     * Creates new form ArrayBotones
     */
    public ArrayBotones() {
        initComponents();
        initComponents2();
        java.awt.KeyboardFocusManager manager = java.awt.KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new MyDispatcher());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(550, 350));
        getContentPane().setLayout(null);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    JButton[] botones;
    JTextField txfPantalla;

    private void initComponents2() {
        botones = new JButton[10];
        for (int i = 0; i < botones.length; i++) {
            botones[i] = new JButton("" + i);
            this.add(botones[i]);
            botones[i].setBounds(50 + 45 * i, 200, 40, 40);
            botones[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    numeroTecleado(e);
                }
            });
        }

        txfPantalla = new JTextField();
        txfPantalla.setBounds(175, 50, 100, 30);
        txfPantalla.setEditable(false);
        txfPantalla.setFont(new java.awt.Font("Tahoma", 0, 18));
        this.add(txfPantalla);
    }

    private void numeroTecleado(ActionEvent evt) {
        JButton btnPulsado = (JButton) evt.getSource();
        txfPantalla.setText(btnPulsado.getText());
    }

    private void pulsarTecla(java.awt.event.KeyEvent evt) {
        char tecla = evt.getKeyChar();
        switch (tecla) {
            case '0':
                txfPantalla.setText("0");
                break;
            case '1':
                txfPantalla.setText("1");
                break;
            case '2':
                txfPantalla.setText("2");
                break;
            case '3':
                txfPantalla.setText("3");
                break;
            case '4':
                txfPantalla.setText("4");
                break;
            case '5':
                txfPantalla.setText("5");
                break;
            case '6':
                txfPantalla.setText("6");
                break;
            case '7':
                txfPantalla.setText("7");
                break;
            case '8':
                txfPantalla.setText("8");
                break;
            case '9':
                txfPantalla.setText("9");
                break;
        }
        
        if(evt.getModifiersEx()==java.awt.event.InputEvent.CTRL_DOWN_MASK && evt.getKeyCode()==java.awt.event.KeyEvent.VK_C){
            txfPantalla.setText("CTRL+C");
        }
    }

    private class MyDispatcher implements java.awt.KeyEventDispatcher {

        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == java.awt.event.KeyEvent.KEY_PRESSED) {
                pulsarTecla(e);
            }
            return false;
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ArrayBotones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ArrayBotones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ArrayBotones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ArrayBotones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ArrayBotones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
