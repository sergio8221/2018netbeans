/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p05_naiperey;

import java.util.ArrayList;
import java.util.Collections;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Sergio Gutiérrez
 */
class Baraja {

    private final ArrayList<Naipe> mazo;
    public static final Icon REVERSO = new ImageIcon("/p05_naiperey/reverso.png");
    public static final Icon REY_OROS = new ImageIcon("/p05_naiperey/12oros.png");
    public static final Icon CABALLO_OROS = new ImageIcon("/p05_naiperey/11oros.png");
    public static final Icon SOTA_OROS = new ImageIcon("/p05_naiperey/10oros.png");
    public static final int OROS = 0;
    public static final int COPAS = 1;
    public static final int ESPADAS = 2;
    public static final int BASTOS = 3;

    public Baraja() {
        mazo = new ArrayList<Naipe>();
        mazo.add(new Naipe(SOTA_OROS, 0, false, OROS, 10));
        mazo.add(new Naipe(CABALLO_OROS, 0, false, OROS, 11));
        mazo.add(new Naipe(REY_OROS, 0, false, OROS, 12));
        barajar();
    }

    public void barajar() {
        Collections.shuffle(mazo);
        for (Naipe n : mazo) {
            n.setRepartido(false);
        }
    }
    
    public Naipe sacarNaipe(){
        
    }
}
