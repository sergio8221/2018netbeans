/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p05_naiperey;

import javax.swing.Icon;

/**
 *
 * @author Sergio Gutiérrez
 */
public class Naipe {
    public static final int ANCHO=80;
    public static final int ALTO=120;
    private final Icon ANVERSO;
    private static final Icon REVERSO=Baraja.REVERSO;
    private final float VALOR;
    private boolean repartido;
    private final int PALO;
    private final int NUMERO;

    public Naipe(Icon ANVERSO, float VALOR, boolean repartido, int PALO, int NUMERO) {
        this.ANVERSO = ANVERSO;
        this.VALOR = VALOR;
        this.repartido = repartido;
        this.PALO = PALO;
        this.NUMERO = NUMERO;
    }

    public static int getANCHO() {
        return ANCHO;
    }

    public static int getALTO() {
        return ALTO;
    }

    public Icon getANVERSO() {
        return ANVERSO;
    }

    public static Icon getREVERSO() {
        return REVERSO;
    }

    public float getVALOR() {
        return VALOR;
    }

    public boolean isRepartido() {
        return repartido;
    }

    public int getPALO() {
        return PALO;
    }

    public int getNUMERO() {
        return NUMERO;
    }

    void setRepartido(boolean b) {
        this.repartido=b;
    }

    
    

    
    
    
}
