/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.Objects;

/**
 *
 * @author Sergio Gutiérrez
 */
public class Contacto {

    //Atributo
    private String nombre;
    private String apellidos;
    private int tlfno;
    private int tlfnMvl;

    //Constructor
    public Contacto(String nombre, String apellidos, int tlfno, int tlfnMvl) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.tlfno = tlfno;
        this.tlfnMvl = tlfnMvl;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public int getTlfno() {
        return tlfno;
    }

    public int getTlfnMvl() {
        return tlfnMvl;
    }

    public void setTlfno(int tlfno) {
        this.tlfno = tlfno;
    }

    public void setTlfnMvl(int tlfnMvl) {
        this.tlfnMvl = tlfnMvl;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.nombre);
        hash = 89 * hash + Objects.hashCode(this.apellidos);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contacto other = (Contacto) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellidos, other.apellidos)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return (nombre+" "+apellidos);
    }
    
    

}
